﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace code2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "HOLA MUNDO DE PCP 1-3";
            label2.Image = Properties.Resources.micraft_de_día;
            label3.Image = Properties.Resources.ícono;
            label1.BackColor = Color.MediumSpringGreen;
            label2.BackColor = Color.MediumSpringGreen;
            label3.BackColor = Color.MediumSpringGreen;
            fondo4.BackColor = Color.MediumSpringGreen;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "ADIÓS MUNDO DE PCP 1-3";
            label2.Image = Properties.Resources.micraft_de_noche;
            label3.Image = Properties.Resources.cubo_herobrine_;
            label1.BackColor = Color.MediumSeaGreen;
            label2.BackColor = Color.MediumSeaGreen;
            label3.BackColor = Color.MediumSeaGreen;
            fondo4.BackColor = Color.MediumSeaGreen;
        }
    }
}
